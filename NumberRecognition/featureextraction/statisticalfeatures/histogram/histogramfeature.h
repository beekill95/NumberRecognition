#ifndef HISTOGRAMFEATURE
#define HISTOGRAMFEATURE

#include "averageentropy.h"
#include "meanhistogram.h"
#include "moment.h"
#include "relativesmoothness.h"
#include "uniformity.h"

#endif // HISTOGRAMFEATURE

